package com.example.covidapp_build_with_java_ver1.API;

import com.example.covidapp_build_with_java_ver1.model.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CovidAPI {
    @GET("countries")
    Call<List<Country>> getCountries();
}
