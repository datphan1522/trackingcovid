package com.example.covidapp_build_with_java_ver1.model;

import java.io.Serializable;

public class info implements Serializable {
    private String name, DoB,created_at;
    private String vaccine_count;
    private boolean has_covid;
    private String userId,infoId;

    public info(){

    }
    public info(String name, String DoB, String created_at, String vaccine_count, boolean has_covid, String userId, String infoId) {
        this.name = name;
        this.DoB = DoB;
        this.created_at = created_at;
        this.vaccine_count = vaccine_count;
        this.has_covid = has_covid;
        this.userId = userId;
        this.infoId = infoId;
    }

    public String getInfoId() {
        return infoId;
    }

    public void setInfoId(String infoId) {
        this.infoId = infoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoB() {
        return DoB;
    }

    public void setDoB(String DoB) {
        this.DoB = DoB;
    }

    public String getcreated_at() {
        return created_at;
    }

    public void setcreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getvaccine_count() {
        return vaccine_count;
    }

    public void setvaccine_count(String vaccine_count) {
        this.vaccine_count = vaccine_count;
    }

    public boolean ishas_covid() {
        return has_covid;
    }

    public void sethas_covid(boolean has_covid) {
        this.has_covid = has_covid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "info: {"
                + "name='" + name + '\''
                + ", DoB='" + DoB + '\''
                + ", created_at='" + created_at + '\''
                + ", vaccine_count='" + vaccine_count + '\''
                + ", has_covid=" + has_covid +'\''
                + ", infoId=" + infoId +'\''
                + '}';
    }
}
