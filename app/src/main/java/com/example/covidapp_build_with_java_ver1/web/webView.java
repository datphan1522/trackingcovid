package com.example.covidapp_build_with_java_ver1.web;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.covidapp_build_with_java_ver1.R;

public class webView extends AppCompatActivity {
    private WebView webView;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_news);

        webView = findViewById(R.id.webview);
        backButton = findViewById(R.id.back_button);

        // Load a webpage in the WebView
        webView.loadUrl("https://covid19.gov.vn/ngay-3-5-co-1201-ca-covid-19-moi-33-benh-nhan-tho-may-171230503200250757.htm");

        // Set up the click listener for the back button
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {
                    webView.goBack();
                } else {
                    finish();
                }
            }
        });
    }

}
