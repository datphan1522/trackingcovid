package com.example.covidapp_build_with_java_ver1.web;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.covidapp_build_with_java_ver1.R;

public class webView2 extends AppCompatActivity {
    private WebView webView;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_prevent);

        webView = findViewById(R.id.webView2);
        backButton = findViewById(R.id.back_button2);

        // Load a webpage in the WebView
        webView.loadUrl("https://covid19.gov.vn/9-bien-phap-moi-nhat-phong-chong-dich-covid-19-nguoi-dan-can-biet-1717073006.htm");

        // Set up the click listener for the back button
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {
                    webView.goBack();
                } else {
                    finish();
                }
            }
        });
    }

}
