package com.example.covidapp_build_with_java_ver1;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class infoUser extends AppCompatActivity implements View.OnClickListener{
    private EditText eName,ePhone,eDoB;
    private TextView eEmail;
    private Button btUpdate,btCancel;

    FirebaseAuth mAuth  = FirebaseAuth.getInstance();;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference infoCollectionRef = db.collection("users");
    String userID = mAuth.getCurrentUser().getUid();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_user);
        initView();

        infoCollectionRef.document(userID).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                eName.setText((String) documentSnapshot.get("Name"));
                eEmail.setText((String) documentSnapshot.get("Mail"));
                ePhone.setText((String) documentSnapshot.get("PhoneNumber"));
                eDoB.setText((String) documentSnapshot.get("DoB"));
            }
        });

        eDoB.setOnClickListener(view -> {
            final Calendar c=Calendar.getInstance();
            int year=c.get(Calendar.YEAR);
            int month=c.get(Calendar.MONTH);
            int day=c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog=new DatePickerDialog(infoUser.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                    String date="";
                    if(m>8){
                        date=d+"/"+(m+1)+"/"+y;
                    }else {
                        date=d+"/0"+(m+1)+"/"+y;
                    }
                    eDoB.setText(date);
                }
            },year,month,day);
            dialog.show();
        });

        btUpdate.setOnClickListener(view -> {
            updateInfo();
            finish();
        });


        btCancel.setOnClickListener(view -> {
            finish();
        });
    }

    private void updateInfo() {
        String name = eName.getText().toString();
//        String mail = eEmail.getText().toString();
        String phone = ePhone.getText().toString();
        String dob= eDoB.getText().toString();


        Map<String, Object> infoData = new HashMap<>();
        infoData.put("Name", name);
//        infoData.put("Mail", mail);
        infoData.put("PhoneNumber",phone);
        infoData.put("DoB", dob);

        infoCollectionRef.document(userID).update(infoData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d("TAG", "onSuccess: ");
                Toast.makeText(infoUser.this, "Update info user success!", Toast.LENGTH_SHORT).show();
            }
        });
        infoCollectionRef.document(userID).update(infoData).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("TAG", "onFailure: ");
                Toast.makeText(infoUser.this, "Update info user failure!", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void initView() {
        eName = findViewById(R.id.txtUser);
        eEmail = findViewById(R.id.txtMail);
        ePhone = findViewById(R.id.txtPhone);
        eDoB = findViewById(R.id.txtDoB);
        btUpdate = findViewById(R.id.btnUpdate);
        btCancel = findViewById(R.id.btnCancel);

    }

    @Override
    public void onClick(View view) {

    }
}
