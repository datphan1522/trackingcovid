package com.example.covidapp_build_with_java_ver1;

import static android.app.PendingIntent.getActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covidapp_build_with_java_ver1.adapter.RecycleViewAdapter;
import com.example.covidapp_build_with_java_ver1.model.info;
import com.example.covidapp_build_with_java_ver1.web.webView;
import com.example.covidapp_build_with_java_ver1.web.webView2;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class Symptom extends AppCompatActivity implements RecycleViewAdapter.ItemListener {

    BottomNavigationView bottomNavigationView;

    private RecycleViewAdapter adapter;
    private RecyclerView recyclerView;
    FirebaseAuth mAuth  = FirebaseAuth.getInstance();;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    CollectionReference infoCollectionRef = db.collection("info");
    String userID = mAuth.getCurrentUser().getUid();
    private FloatingActionButton fab;
    private ImageView info1, webView1,webView2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptom);

        recyclerView=findViewById(R.id.recycleView);
        adapter=new RecycleViewAdapter();
        bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setBackground(null);
        bottomNavigationView.setSelectedItemId(R.id.nav_symptom);
        bottomNavigationView.setItemIconTintList(null);
        fab = findViewById(R.id.fab);
        info1 = findViewById(R.id.info1);
        webView1 = findViewById(R.id.webview1);
        webView2 = findViewById(R.id.webview2);


        List<info> list = new ArrayList<>();
        infoCollectionRef.whereEqualTo("userId", userID).get()
            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        String Located = (String) documentSnapshot.get("Located");
                        String DoB = (String) documentSnapshot.get("DoB");
                        String created_at = (String) documentSnapshot.get("created_at");
                        String vaccine_count = (String) documentSnapshot.get("vaccine_count");
                        Boolean has_covid = (Boolean) documentSnapshot.get("has_covid");
                        String infoId = (String) documentSnapshot.getId();

                        info a = new info();
                        a.setName(Located);
                        a.setDoB(DoB);
                        a.setcreated_at(created_at);
                        a.setvaccine_count(vaccine_count);
                        a.sethas_covid(has_covid);
                        a.setInfoId(infoId);
                        list.add((info) a);
                    }
                    adapter.setList(list);
                    LinearLayoutManager manager =new LinearLayoutManager(Symptom.this,RecyclerView.VERTICAL,false);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(adapter);
                    adapter.setItemListener(Symptom.this);

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("TAG", "onFailure: ");
                }
            });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Symptom.this,addActivity.class);
                startActivity(intent);
            }
        });

        info1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Symptom.this,infoUser.class);
                startActivity(intent);
            }
        });

        webView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Symptom.this, webView.class);
                startActivity(intent);
            }
        });
        webView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Symptom.this, webView2.class);
                startActivity(intent);
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplicationContext(),HomePage.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_search:
                        startActivity(new Intent(getApplicationContext(),cityFinder.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_symptom:

                        return true;

                }
                return false;
            }
        });
    }



    @Override
    public void onItemClick(View view, int position) {
        info item=adapter.getItem(position);
//        Log.d("TAG","item: " + item );
        Intent intent=new Intent(Symptom.this, UpdateDeleteActivity.class);
        intent.putExtra("item",item);
        startActivity(intent);
    }

    public void onResume() {
        super.onResume();
        List<info> list = new ArrayList<>();
        infoCollectionRef.whereEqualTo("userId", userID).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            String Located = (String) documentSnapshot.get("Located");
                            String DoB = (String) documentSnapshot.get("DoB");
                            String created_at = (String) documentSnapshot.get("created_at");
                            String vaccine_count = (String) documentSnapshot.get("vaccine_count");
                            Boolean has_covid = (Boolean) documentSnapshot.get("has_covid");
                            String infoId = (String) documentSnapshot.getId();

                            info a = new info();
                            a.setName(Located);
                            a.setDoB(DoB);
                            a.setcreated_at(created_at);
                            a.setvaccine_count(vaccine_count);
                            a.sethas_covid(has_covid);
                            a.setInfoId(infoId);
                            list.add((info) a);
                        }
                        adapter.setList(list);
                    }

                    ;
                });
    }
}