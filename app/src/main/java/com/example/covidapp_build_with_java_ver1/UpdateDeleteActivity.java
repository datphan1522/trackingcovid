package com.example.covidapp_build_with_java_ver1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.covidapp_build_with_java_ver1.model.info;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class UpdateDeleteActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText eName, eDOB, eDateCreate, eNumVacxin;
    private CheckBox cCovid;
    private Button btUpdate,btDelete,btCancel;
    FirebaseAuth mAuth  = FirebaseAuth.getInstance();;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference infoCollectionRef = db.collection("info");
    String userID;
    private info item;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete_item);
        initView();

        Intent intent=getIntent();
        item=(info) intent.getSerializableExtra("item");

        eName.setText(item.getName());
        eDOB.setText(item.getDoB());
        eDateCreate.setText(item.getcreated_at());
        eNumVacxin.setText(item.getvaccine_count());
        cCovid.setChecked(item.ishas_covid());
//        Log.d("TAG2", "infoId: "+ item.getInfoId());


        eDOB.setOnClickListener(view -> {
            final Calendar c=Calendar.getInstance();
            int year=c.get(Calendar.YEAR);
            int month=c.get(Calendar.MONTH);
            int day=c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog=new DatePickerDialog(UpdateDeleteActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                    String date="";
                    if(m>8){
                        date=d+"/"+(m+1)+"/"+y;
                    }else {
                        date=d+"/0"+(m+1)+"/"+y;
                    }
                    eDOB.setText(date);
                }
            },year,month,day);
            dialog.show();
        });

        eDateCreate.setOnClickListener(view -> {
            final Calendar c=Calendar.getInstance();
            int year=c.get(Calendar.YEAR);
            int month=c.get(Calendar.MONTH);
            int day=c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog=new DatePickerDialog(UpdateDeleteActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                    String date="";
                    if(m>8){
                        date=d+"/"+(m+1)+"/"+y;
                    }else {
                        date=d+"/0"+(m+1)+"/"+y;
                    }
                    eDateCreate.setText(date);
                }
            },year,month,day);
            dialog.show();
        });

        btUpdate.setOnClickListener(view -> {
            updateInfo();
            finish();
        });

        btDelete.setOnClickListener(view -> {
            deleteInfo();
            finish();
        });

        btCancel.setOnClickListener(view -> {
            finish();
        });
    }

    private void deleteInfo() {
        infoCollectionRef.document(item.getInfoId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d("TAG", "onSuccess: ");
                Toast.makeText(UpdateDeleteActivity.this, "Delete info user success!", Toast.LENGTH_SHORT).show();
            }
        });
        infoCollectionRef.document(item.getInfoId()).delete().addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("TAG", "onFailure: ");
                Toast.makeText(UpdateDeleteActivity.this, "Delete info user failure!", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void updateInfo() {
        String name=eName.getText().toString();
        String dob=eDOB.getText().toString();
        String eDate=eDateCreate.getText().toString();
        String num=eNumVacxin.getText().toString();
        boolean c= cCovid.isChecked();
        userID = mAuth.getCurrentUser().getUid();

        Log.d("TAG1",userID);

        Map<String, Object> infoData = new HashMap<>();
        infoData.put("Located", name);
        infoData.put("DoB", dob);
        infoData.put("vaccine_count", num);
        infoData.put("has_covid", c);
        infoData.put("created_at",eDate);
        infoData.put( "userId", userID);

        infoCollectionRef.document(item.getInfoId()).update(infoData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d("TAG", "onSuccess: ");
                Toast.makeText(UpdateDeleteActivity.this, "Update info user success!", Toast.LENGTH_SHORT).show();
            }
        });
        infoCollectionRef.document(item.getInfoId()).update(infoData).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("TAG", "onFailure: ");
                Toast.makeText(UpdateDeleteActivity.this, "Update info user failure!", Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void initView() {
        eName= findViewById(R.id.tvName);
        eDOB= findViewById(R.id.tvDOB);
        eDateCreate=findViewById(R.id.tvDateCreate);
        eNumVacxin= findViewById(R.id.tvNumVacxin);
        cCovid=findViewById(R.id.checkbox1);
        btUpdate=findViewById(R.id.btUpdate);
        btDelete=findViewById(R.id.btDelete);
        btCancel=findViewById(R.id.btCancel);
    }

    @Override
    public void onClick(View view) {

    }
}
