package com.example.covidapp_build_with_java_ver1;

import android.app.DatePickerDialog;
import android.content.ClipData;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.covidapp_build_with_java_ver1.model.info;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class addActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText eName, eDOB, eDateCreate, eNumVacxin;
    private CheckBox cCovid;
    private Button btUpdate,btCancel;

    FirebaseAuth mAuth  = FirebaseAuth.getInstance();;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference infoCollectionRef = db.collection("info");
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        initView();

        eDOB.setOnClickListener(view -> {
            final Calendar c=Calendar.getInstance();
            int year=c.get(Calendar.YEAR);
            int month=c.get(Calendar.MONTH);
            int day=c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog=new DatePickerDialog(addActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                    String date="";
                    if(m>8){
                        date=d+"/"+(m+1)+"/"+y;
                    }else {
                        date=d+"/0"+(m+1)+"/"+y;
                    }
                    eDOB.setText(date);
                }
            },year,month,day);
            dialog.show();
        });

        eDateCreate.setOnClickListener(view -> {
            final Calendar c=Calendar.getInstance();
            int year=c.get(Calendar.YEAR);
            int month=c.get(Calendar.MONTH);
            int day=c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog=new DatePickerDialog(addActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                    String date="";
                    if(m>8){
                        date=d+"/"+(m+1)+"/"+y;
                    }else {
                        date=d+"/0"+(m+1)+"/"+y;
                    }
                    eDateCreate.setText(date);
                }
            },year,month,day);
            dialog.show();
        });


        btUpdate.setOnClickListener(view -> {
            createInfo();
            finish();
        });


        btCancel.setOnClickListener(view -> {
            finish();
        });

    }

    private void createInfo() {
        String name=eName.getText().toString();
        String dob=eDOB.getText().toString();
        String eDate=eDateCreate.getText().toString();
        String num=eNumVacxin.getText().toString();
        boolean c= cCovid.isChecked();
        userID = mAuth.getCurrentUser().getUid();

        Log.d("TAG1",userID);

        Map<String, Object> infoData = new HashMap<>();
        infoData.put("Located", name);
        infoData.put("DoB", dob);
        infoData.put("vaccine_count", num);
        infoData.put("has_covid", c);
        infoData.put("created_at",eDate);
        infoData.put( "userId", userID);

        infoCollectionRef.add(infoData);

        infoCollectionRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d("TAG", document.getId() + " => " + document.getData());
                    }
                } else {
                    Log.w("TAG", "Error getting documents.", task.getException());
                }
            }
        });
    }

    private void initView() {
        eName= findViewById(R.id.tvName);
        eDOB= findViewById(R.id.tvDOB);
        eDateCreate=findViewById(R.id.tvDateCreate);
        eNumVacxin= findViewById(R.id.tvNumVacxin);
        cCovid=findViewById(R.id.checkbox1);
        btUpdate=findViewById(R.id.btUpdate);
        btCancel=findViewById(R.id.btCancel);
    }

    @Override
    public void onClick(View view) {

//
//        if(view==eDOB){
//            final Calendar c=Calendar.getInstance();
//            int year=c.get(Calendar.YEAR);
//            int month=c.get(Calendar.MONTH);
//            int day=c.get(Calendar.DAY_OF_MONTH);
//            DatePickerDialog dialog=new DatePickerDialog(addActivity.this, new DatePickerDialog.OnDateSetListener() {
//                @Override
//                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
//                    String date="";
//                    if(m>8){
//                        date=d+"/"+(m+1)+"/"+y;
//                    }else {
//                        date=d+"/0"+(m+1)+"/"+y;
//                    }
//                    eDOB.setText(date);
//                }
//            },year,month,day);
//            dialog.show();
//        }
//
//        if(view==eDateCreate){
//            final Calendar c=Calendar.getInstance();
//            int year=c.get(Calendar.YEAR);
//            int month=c.get(Calendar.MONTH);
//            int day=c.get(Calendar.DAY_OF_MONTH);
//            DatePickerDialog dialog=new DatePickerDialog(addActivity.this, new DatePickerDialog.OnDateSetListener() {
//                @Override
//                public void onDateSet(DatePicker datePicker, int y, int m, int d) {
//                    String date="";
//                    if(m>8){
//                        date=d+"/"+(m+1)+"/"+y;
//                    }else {
//                        date=d+"/0"+(m+1)+"/"+y;
//                    }
//                    eDateCreate.setText(date);
//                }
//            },year,month,day);
//            dialog.show();
//        }
//
//        if(view==btCancel){
//            finish();
//        }
//        if (view==btUpdate){
//            String name=eName.getText().toString();
//            String dob=eDOB.getText().toString();
//            String eDate=eDateCreate.getText().toString();
//            String num=eNumVacxin.getText().toString();
//            boolean c= cCovid.isChecked();
//            userID = mAuth.getCurrentUser().getUid();
//
//            Log.d("TAG1",userID);
//
//            Map<String, Object> infoData = new HashMap<>();
//            infoData.put("name", name);
//            infoData.put("DoB", dob);
//            infoData.put("vaccine_count", num);
//            infoData.put("has_covid", c);
//            infoData.put("created_at",eDate);
//
//
//            infoCollectionRef.add(infoData);
//
//            infoCollectionRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                @Override
//                public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                    if (task.isSuccessful()) {
//                        for (QueryDocumentSnapshot document : task.getResult()) {
//                            Log.d("TAG", document.getId() + " => " + document.getData());
//                        }
//                    } else {
//                        Log.w("TAG", "Error getting documents.", task.getException());
//                    }
//                }
//            });
//
//        }

    }

    //getAll info

}
