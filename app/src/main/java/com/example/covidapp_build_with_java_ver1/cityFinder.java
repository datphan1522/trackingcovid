package com.example.covidapp_build_with_java_ver1;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.covidapp_build_with_java_ver1.API.CovidAPI;
import com.example.covidapp_build_with_java_ver1.model.Country;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class cityFinder extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    TextView Button_VietNam, Button_China, Button_Us, Button_Uk, Button_India,
    Button_Iran, Button_Egypt, Button_Sk;
    AutoCompleteTextView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_finder);
        searchView = findViewById(R.id.searchCity);
//        final EditText editText = findViewById(R.id.searchCity);

        Button_VietNam = findViewById(R.id.button_vn);
        Button_China = findViewById(R.id.button_china);
        Button_Us = findViewById(R.id.button_us);
        Button_Uk = findViewById(R.id.button_uk);
        Button_India = findViewById(R.id.button_india);
        Button_Egypt = findViewById(R.id.button_egypt);
        Button_Sk = findViewById(R.id.button_south_korea);
        Button_Iran = findViewById(R.id.button_iran);


        bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setBackground(null);
        bottomNavigationView.setSelectedItemId(R.id.nav_search);
        bottomNavigationView.setItemIconTintList(null);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplicationContext(),HomePage.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_search:
                        return true;
                    case R.id.nav_symptom:
                        startActivity(new Intent(getApplicationContext(),Symptom.class));
                        overridePendingTransition(0,0);
                        return true;

                }
                return false;
            }
        });

        //---------------------------------
        // Tạo đối tượng Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://disease.sh/v3/covid-19/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Tạo đối tượng CovidAPI từ Retrofit
        CovidAPI covidAPI = retrofit.create(CovidAPI.class);

        // Gọi API để lấy danh sách quốc gia
        Call<List<Country>> call = covidAPI.getCountries();

        // Thực hiện cuộc gọi đồng bộ
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                Log.d("TAG1", "onResponse: "+ response.toString());
                List<Country> countries = response.body();
                List<String> countryNames = new ArrayList<>();

                // Lấy tên của từng country và thêm vào danh sách countryNames
                for (Country country : countries) {
                    String countryName = country.getCountry();
                    countryNames.add(countryName);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>( cityFinder.this, android.R.layout.simple_list_item_1, countryNames);
                searchView.setAdapter(adapter);
                searchView.setThreshold(1);
                searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedPlace = (String) parent.getItemAtPosition(position);
                        searchView.setText(selectedPlace);
                        Log.d("LOCATION", "onItemClick: "+ selectedPlace);
                    }
                });
//                    Toast.makeText(MainActivity.this, "Data saved to Firebase", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {

            }
        });

//            @Override
//            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Country>> call, Throwable t) {
//                Toast.makeText(MainActivity.this, "Failed to fetch data from API", Toast.LENGTH_SHORT).show();
//            }
//        });


        searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String newCity = searchView.getText().toString();
                Log.d("TAG11", "onEditorAction: "+newCity);
                Intent intent = new Intent(cityFinder.this, HomePage.class);
                intent.putExtra("City", newCity);
                startActivity(intent);
                return false;
            }
        });
        Button_VietNam.setOnClickListener(view -> {
            findCountry("VietNam");
        });
        Button_China.setOnClickListener(view -> {
            findCountry("China");
        });
        Button_Uk.setOnClickListener(view -> {
            findCountry("United Kingdom");
        });
        Button_Us.setOnClickListener(view -> {
            findCountry("United States");
        });
        Button_India.setOnClickListener(view -> {
            findCountry("India");
        });
        Button_Iran.setOnClickListener(view -> {
            findCountry("Iran");
        });
        Button_Egypt.setOnClickListener(view -> {
            findCountry("Egypt");
        });
        Button_Sk.setOnClickListener(view -> {
            findCountry("South Korea");
        });



    }

    private void findCountry(String country) {
        Intent intent = new Intent(this,HomePage.class);
        intent.putExtra("City",country);
        startActivity(intent);
    }

}
