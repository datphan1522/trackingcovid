package com.example.covidapp_build_with_java_ver1.adapter;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covidapp_build_with_java_ver1.R;
import com.example.covidapp_build_with_java_ver1.model.info;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.HomeViewHolder>{
    private List<info> list;
    private ItemListener itemListener;

    public void setItemListener(ItemListener itemListener) {
        this.itemListener = itemListener;
        notifyDataSetChanged();
    }

    public RecycleViewAdapter() {
        list = new ArrayList<>();
    }

    public void setList(List<info> list) {
        this.list = list;
        notifyDataSetChanged();
    }
    public info getItem(int positon){
//        Log.d("TAGX", "getItem: "+ list.get(positon));
        return list.get(positon);
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        info item = list.get(position);
        holder.Located.setText("  " +item.getName());
        holder.DoB.setText("  "+item.getDoB());
        holder.dateCreate.setText(item.getcreated_at());
        holder.numVacxin.setText("Số mũi tiêm: "+item.getvaccine_count());
        holder.checkBox1.setText(item.ishas_covid()?"Đã bị Covid":"Chưa bị Covid");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView Located,DoB,dateCreate,numVacxin,checkBox1;

        public HomeViewHolder(@NonNull View view) {
            super(view);
            Located= view.findViewById(R.id.tviname);
            DoB = view.findViewById(R.id.tviDoB);
            dateCreate = view.findViewById(R.id.tviDateCreate);
            numVacxin = view.findViewById(R.id.tviNumVacxin);
            checkBox1 = view.findViewById(R.id.tviHascovid);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(itemListener!=null){
                itemListener.onItemClick(view,getLayoutPosition());
            }
        }
    }
    public interface ItemListener{
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public String toString() {
        return list.toString();
    }
}
